#!/bin/sh
# This script is intended to be run on the camera-build docker shell
export PKG_CONFIG_PATH=/build/build-yocto-arm/tmp/sysroots-components/aarch64/systemd/usr/lib/pkgconfig
cargo build --target aarch64-unknown-linux-gnu --release
