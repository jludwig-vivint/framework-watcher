# Framework Service Watcher

## Description

Systmed serice that watches for framework service process and dumps the framework log when framework_app stops

## Build

#### Aarch64 Build from Camera Docker Shell

1. Copy or clone the `firmware-watcher` directory to the camera-build workspace root.
2. Run the camera docker shell `./docker_shell.sh`.
3. Change to the `firmware-watcher` directory.
4. Run `./build-arm.sh` in the framework-watcher project root directory.
