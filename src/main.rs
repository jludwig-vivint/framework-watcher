/// Service to save shutdown logs of the framework service whenever the service process terminates
///
/// Currently there is no distinction between a crash and a graceful shutdown.
/// The shutdown logs are stored in /mnt/media/. The latest 24 shutdown logs will be retained.
///
use anyhow::Result;
use chrono::{DateTime, Local, Utc};
use env_logger::Builder;
use futures::stream::StreamExt;
use glob::glob;
use log::{debug, error, info, trace, warn};
use signal_hook::consts::signal::*;
use signal_hook_tokio::Signals;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use sysinfo::{Pid, System};
use systemd::journal;
use tokio::fs::{self, File};
use tokio::io::AsyncWriteExt;
use tokio::time;

#[cfg(test)]
const LOG_FILENAME_BASE: &str = "framework-stop-";
#[cfg(not(test))]
/// Base name for process shutdown logs
const LOG_FILENAME_BASE: &str = "/mnt/media/framework-stop-";

/// The maximum number of shutdown log files to retain
const MAX_LOG_FILE_COUNT: usize = 24;

/// Returns a timestamp of the current time in seconds since the UNIX epoch
///
/// # Returns
/// The number of seconds from the UNIX epoch to now
///
/// # Errors
/// Fails if there is an error getting the time since the UNIX epoch
fn timestamp() -> Result<u64> {
    let now = SystemTime::now();
    Ok(now
        .duration_since(UNIX_EPOCH)
        .map_err(|e| {
            warn!("Error getting duration since eopch: {e}");
            e
        })?
        .as_secs())
}

/// Dumps a system log to a file
///
/// # Arguments
/// * `unit_name` - Optional unit name. Output from all will be acptured logs if None
/// * `output_filename` - The name of the file in which to store the output
///
/// # Errors
/// * The journal log reader cannot be opened
/// * A unit name match cannot be added to the reader
/// * Error seeking to the head of the log
/// * Error writing to the ouptut file
async fn dump_system_log(unit_name: Option<&str>, output_filename: &str) -> Result<()> {
    let unit_name_str = unit_name.unwrap_or("system");

    info!("Dumping {unit_name_str} log to {output_filename}...");
    let mut reader = journal::OpenOptions::default().open().map_err(|e| {
        error!("Failed to set open systemd journal: {e}");
        e
    })?;

    if let Some(unit_name_str) = unit_name {
        reader
            .match_add("_SYSTEMD_UNIT", unit_name_str)
            .map_err(|e| {
                error!("Failed to set systemd match filter _SYSTEMD_UNIT=>{unit_name_str}: {e}");
                e
            })?;
    }

    // Start from the beginning of the log
    reader.seek(journal::JournalSeek::Head).map_err(|e| {
        error!("Failed to seek to head of systemd journal: {e}");
        e
    })?;

    let mut file = File::create(output_filename).await.map_err(|e| {
        error!("Failed to create log output file \"{output_filename}\": {e}");
        e
    })?;

    loop {
        match reader.next_entry()? {
            Some(entry) => {
                let datetime: DateTime<Utc> = reader
                    .timestamp()
                    .map_err(|e| {
                        error!("Failed to get log entry timestamp: {e}");
                        e
                    })?
                    .into();
                let timestamp = datetime
                    .with_timezone(&Local)
                    .format("%b %d %T")
                    .to_string();
                trace!("timestamp - {timestamp}:");
                for key in entry.keys() {
                    if let Some(value) = entry.get(key) {
                        trace!("{key} => \"{value}\"");
                    } else {
                        trace!("{key} => null");
                    }
                }
                if let Some(message) = entry.get("MESSAGE") {
                    let hostname = match entry.get("_HOSTNAME") {
                        Some(value) => value.to_owned() + " ",
                        None => String::new(),
                    };
                    let syslog_id = match entry.get("SYSLOG_IDENTIFIER") {
                        Some(value) => value,
                        None => "",
                    };
                    let pid = match entry.get("_PID") {
                        Some(value) => "[".to_string() + value + "]",
                        None => String::new(),
                    };
                    let log_message = format!("{timestamp} {hostname}{syslog_id}{pid}: {message}");
                    debug!("{log_message}");
                    file.write_all(format!("{log_message}\n").as_bytes())
                        .await.map_err(|e| {
                            error!("Failed to write log entry to log output file \"{output_filename}\": {e}");
                            e
                        })?;
                    if let Err(e) = file.flush().await {
                        warn!("Error flushing log output file \"{output_filename}\": {e}");
                    }
                }
            }
            None => {
                info!("Successfully dumped {unit_name_str} log to {output_filename}");
                return Ok(());
            }
        }
    }
}

/// Waits for a process with a given binary name to start and then exit
///
/// This function will not wait for a process start if the process is already running.
///
/// # Arguments
/// * `process_name` - The name of the process to wait for
/// * `running` - Flag to use to check for program/service exit
async fn wait_for_process_start_and_exit(process_name: &str, running: Arc<AtomicBool>) {
    let mut system = System::new();

    let mut pid: Option<Pid> = None;

    info!("Waiting for process \"{process_name}\" to start...");
    while running.load(Ordering::SeqCst) {
        system.refresh_processes();
        match pid {
            None => {
                // Waiting for process to start
                for (process_pid, process) in system.processes() {
                    if process.name() == process_name {
                        info!("Process \"{process_name}\" started with PID {process_pid}");
                        pid = Some(*process_pid);
                        break;
                    }
                }
            }
            Some(pid) => {
                // Waitiing for process to end
                if system.process(pid).is_none() {
                    info!("Process \"{process_name}\":{} terminated", pid);
                    break;
                }
            }
        };

        time::sleep(Duration::from_secs(1)).await;
    }
}

/// Deletes older logs keeping the latest MAX_LOG_FILE_COUNT log files
///
/// # Errors
/// * Failuring listing current list of log files
/// * Logs errors, but does not fail if there is an error deleting a log file
async fn tiddy_logs() -> Result<()> {
    info!("Deleting old log files...");
    let mut log_files: Vec<_> = glob(&format!("{LOG_FILENAME_BASE}*"))?
        .filter_map(Result::ok)
        .collect();
    log_files.sort();
    // Keep the last MAX_LOG_FILE_COUNT log files.
    if log_files.len() > MAX_LOG_FILE_COUNT {
        log_files.drain(log_files.len() - MAX_LOG_FILE_COUNT..);
        for log_file in log_files {
            let log_file_path = log_file.to_str().unwrap_or("UNPRINTABLE_FILE_NAME");
            debug!("Deleting old log file {log_file_path}...");
            if let Err(err) = fs::remove_file(log_file.clone()).await {
                error!("Error deleting old log file \"{}\": {err}", log_file_path);
            }
        }
    }
    Ok(())
}

/// Handles service exit symbols
///
/// Signals the rest of the process via the running flag that the process must exit
///
/// # Arguments
/// * `signals` - A handle on which to listen for signals
/// * `running` - A flag used to signal to the rest of the process that the process must exit
async fn handle_signals(mut signals: Signals, running: Arc<AtomicBool>) {
    while let Some(signal) = signals.next().await {
        match signal {
            SIGTERM | SIGINT | SIGQUIT => {
                running.store(false, Ordering::SeqCst);
            }
            _ => unreachable!(),
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    let env = env_logger::Env::default().default_filter_or("info");
    let mut builder = Builder::from_env(env);
    builder.format_timestamp_millis().init();

    info!("Framework watcher starting");

    let running = Arc::new(AtomicBool::new(true));

    let signals = Signals::new([SIGTERM, SIGINT, SIGQUIT])?;
    let handle = signals.handle();
    let signals_task = tokio::spawn(handle_signals(signals, running.clone()));

    if let Err(e) = tiddy_logs().await {
        warn!("Error tidying logs: {e}");
    }

    while running.load(Ordering::SeqCst) {
        // Wait for the framework process to start and then stop
        wait_for_process_start_and_exit("framework_app", running.clone()).await;

        if !running.load(Ordering::SeqCst) {
            break;
        }

        // Write the framework logs to a log file
        let filename = format!("{LOG_FILENAME_BASE}{}.log", timestamp().unwrap_or(0));
        dump_system_log(Some("framework.service"), &filename)
            .await
            .map_err(|e| {
                error!("Error dumping framework system log: {e}");
                e
            })?;
    }

    handle.close();
    signals_task.await?;
    info!("Framework watcher exiting");
    Ok(())
}

#[cfg(test)]
mod tests {

    use env_logger::Builder;
    use glob::glob;
    use log::warn;
    use std::io;
    use tokio::fs::{self, File};

    use super::{tiddy_logs, LOG_FILENAME_BASE};

    fn setup_logging() {
        let env = env_logger::Env::default().default_filter_or("info");
        let mut builder = Builder::from_env(env);
        builder.format_timestamp_millis().init();
    }

    async fn cleanup_test_files() {
        // Get a list of matching file paths
        let file_paths: Vec<_> = glob(&(LOG_FILENAME_BASE.to_string() + "*"))
            .expect("Failed to find old log files")
            .filter_map(Result::ok)
            .collect();

        // Delete each file asynchronously
        for path in file_paths {
            if let Err(err) = fs::remove_file(path).await {
                warn!("Error deleting file: {}", err);
            }
        }
    }

    async fn file_exits(filename: &str) -> bool {
        match fs::metadata(&filename).await {
            Ok(medatata) => medatata.is_file(),
            Err(err) => match err.kind() {
                io::ErrorKind::NotFound => false,
                _ => {
                    warn!("Error check if file \"{filename}\" exists: {err}");
                    false
                }
            },
        }
    }

    fn test_file_name(i: i32) -> String {
        format!("{LOG_FILENAME_BASE}{i:02}.log")
    }

    #[tokio::test]
    async fn test_tidy_logs() {
        setup_logging();

        for i in 0..36 {
            let filename = test_file_name(i);
            File::create(filename)
                .await
                .expect("Failed to create test log file");
        }

        tiddy_logs().await.expect("Failed to tidy log files");

        for i in 0..12 {
            let filename = test_file_name(i);
            assert!(!file_exits(&filename).await);
        }

        for i in 12..36 {
            let filename = test_file_name(i);
            assert!(file_exits(&filename).await);
        }

        cleanup_test_files().await;
    }
}
